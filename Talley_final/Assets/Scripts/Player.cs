﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour {

    public float restartLevelDelay = 1f;
    public float speed;             //Floating point variable to store the player's movement speed.
    public Text countText;
    //public Text popupText;
    public Text instrustionsText;

    private Rigidbody2D rb;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
    private int food = 0;
    public AudioSource Playeraudio;
    public AudioClip foodSound;
    public AudioClip slipSound;
    public AudioClip wooSound;
    public AudioClip lostSound;


    // Use this for initialization
    void Start()
    {
        //Get and store a reference to the Rigidbody2D component so that we can access it.
        rb = GetComponent<Rigidbody2D>();
        SetCountText();
        /*instrustionsText.text = "Your wildest grocery store visit has yet to come. Your mom sent you to grab groceries from the local store. Little does she know, you've been banned. Try grabbing all the items off your mom's list while staying clear of angry managers and wet floors.";
        Time.timeScale = 0;*/

        // audio = GetComponent<AudioSource>();

        //Get a reference to our text LevelText's text component by finding it by name and calling GetComponent.
        //popupText = GameObject.Find("popUp").GetComponent<Text>();
    }

    /*
    //Start overrides the Start function of MovingObject
    protected override void Start ()
    {
        //Get the current food point total stored in GameManager.instance between levels.
        food = GameManager.instance.count;

        //Call the Start function of the MovingObject base class.
        base.Start();
    }
    

    //This function is called when the behaviour becomes disabled or inactive.
    private void OnDisable()
    {
        //When Player object is disabled, store the current local food total in the GameManager so it can be re-loaded in next level.
        GameManager.instance.playerFoodPoints = food;
    }
    */


    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void FixedUpdate()
    {
        //Store the current horizontal input in the float moveHorizontal.
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Store the current vertical input in the float moveVertical.
        float moveVertical = Input.GetAxis("Vertical");

        //Use the two store floats to create a new Vector2 variable movement.
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
        rb.AddForce(movement * speed);

        /*
        if (Input.anyKeyDown)
        {
            Time.timeScale = 1;
            Destroy(gameObject);
        

        audio.Play();
        }*/
    }

    //OnTriggerEnter2D is called whenever this object overlaps with a trigger collider.
    void OnTriggerEnter2D(Collider2D other)
    {

        //Check if the tag of the trigger collided with is Exit.
        if (other.tag == "Exit")
        {
            //Invoke the Restart function to start the next level with a delay of restartLevelDelay (default 1 second).
            Invoke("Restart", restartLevelDelay);

            //Disable the player object since level is over.
            enabled = false;
        }


        //Check the provided Collider2D parameter other to see if it is tagged "food", if it is...
        if (other.gameObject.CompareTag("Food"))
        {
            Destroy(other.gameObject);
            /*other.gameObject.SetActive(false);*/
            GameManager.instance.count = GameManager.instance.count + 20;

            Playeraudio.PlayOneShot(foodSound);

            //Call our SetCountText function which will update the text with the current value for count.
            SetCountText();

            //pop up text
           // popupText.text = "Mom's going to love this!";

          
        }

        //When the player goes over water, they should lose all their points(money)
        if (other.gameObject.CompareTag("Water"))
        {
            other.gameObject.SetActive(false);
            GameManager.instance.count -= 10;

            Playeraudio.PlayOneShot(slipSound);

            //Call our SetCountText function which will update the text with the current value for count.
            SetCountText();

            //text when slip
            //popupText.text = "Crap, lost 10 bucks.";
        }

        //When the player goes over money, they should gain more points(money)
        if (other.gameObject.CompareTag("Money"))
        {
            other.gameObject.SetActive(false);
            GameManager.instance.count = GameManager.instance.count + 100;

            Playeraudio.PlayOneShot(wooSound);

            //Call our SetCountText function which will update the text with the current value for count.
            SetCountText();

            //FOUND MONEY
          //  popupText.text = "WOOHOO";
        }

        //When the player runs into a manager, they will lose all their points and restart the level
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.SetActive(false);
            GameManager.instance.count = 0;

            Playeraudio.PlayOneShot(lostSound);

            //Call our SetCountText function which will update the text with the current value for count.
            SetCountText();

            //Reload/restart level text
            //Invoke("Restart", restartLevelDelay);
            // Destroy(GameManager.instance);
            SceneManager.LoadScene(0);
            GameManager.instance.GameOver();

            //Disable the player object since level is over.
            enabled = false;

            //Set levelText to display number of levels passed and game over message
            GameManager.instance.levelText.text = "Game Restart.";
        }
  

    }


    //Restart reloads the scene when called.
    private void Restart() {
        GameManager.instance.Restart();
    }

    void SetCountText()
    {
        countText.text = "Cash: $ " + GameManager.instance.count.ToString();

    }


    //CheckIfGameOver checks if the player is out of food points and if so, ends the game.
    private void CheckIfGameOver()
    {
        //Check if food point total is less than or equal to zero.
        if (food <= 0)
        {

            //Call the GameOver function of GameManager.
            GameManager.instance.GameOver();
        }
    }
}