﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListDisplay : MonoBehaviour {

    public List<GameObject> listPool;
    public Text groceryList;


    // Use this for initialization
    void Awake () {

        //Adding objects to list
        listPool = new List<GameObject>();

        groceryList = GameObject.FindGameObjectWithTag("groceryList").GetComponent<Text>();
        print(groceryList.text);

    }

    void Update()
    {
        string groceriesString = "Grocery List \n \n";

        for (int i = 0; i < listPool.Count; i++)
        {
            string groceryName = listPool[i].GetComponent<listPickUp>().groceryName;
            groceriesString += groceryName;
            groceriesString += "\n";
        }

        groceryList.text = groceriesString;
    }
}
