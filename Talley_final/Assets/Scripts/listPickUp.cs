﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class listPickUp : MonoBehaviour {

    ListDisplay gm;
    public string groceryName = "";

	// Use this for initialization
	void Start () {
        GameObject controllerObj = GameObject.FindGameObjectWithTag("groceryList");
        gm = controllerObj.GetComponent<ListDisplay>();
        gm.listPool.Add(gameObject);
		
	}

    private void OnDestroy()
    {
        for(int i = 0; i < gm.listPool.Count; i++)
        {
            if (gm.listPool[i] == gameObject)
            {
                gm.listPool.RemoveAt(i);
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
